package com.findmyfriend.ps.findmyfriend.views.dashboard.abas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findmyfriend.ps.findmyfriend.BaseFragment;
import com.findmyfriend.ps.findmyfriend.R;

/**
 * Created by pedro on 05/05/2017.
 */

public class TeamFragment extends BaseFragment {

    public static TeamFragment newInstance() {

        Bundle args = new Bundle();

        TeamFragment fragment = new TeamFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_team, container, false);

        return rootView;
    }

}
