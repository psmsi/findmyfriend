package com.findmyfriend.ps.findmyfriend;

import android.content.Intent;
import android.os.Bundle;

import com.findmyfriend.ps.findmyfriend.views.dashboard.DashboardActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class LauncherActivity extends BaseActivity {

    private static final int TIME_OF_LAUNCHER = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        getSupportActionBar().hide();

        Single.just("delay").delay(TIME_OF_LAUNCHER, TimeUnit.SECONDS).subscribe(new Consumer<String>() {
            @Override
            public void accept(@NonNull String s) throws Exception {
                Intent intent = new Intent(LauncherActivity.this, DashboardActivity.class);
                LauncherActivity.this.startActivity(intent);
                LauncherActivity.this.finish();
            }
        });
    }
}
